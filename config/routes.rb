Rails.application.routes.draw do
  get 'deployment/index'
#  devise_for :users, :path_names => { :sign_up => "register" }
  devise_for :users
  root 'welcome#index'
  # Create Index (form) & Post (creation)
  get '/create', :to => 'create#index'
  post '/create', :to => 'create#post'
  # Show VM routes 
  get '/show/:id' => 'show#index'
  get '/show/stop/:id' => 'show#stop'
  delete '/show/:id' => 'show#destroy'
  # Edit VM routes
  get '/edit/:id', :to => 'edit#index'
  post '/edit/:id', :to => 'edit#post'
  # N.O.C routes
  get '/ops', :to => 'ops#index'
  # Notes resources CRUD
  resources :notes
  # Deploy controller
  get '/deployment', :to => 'deployment#index'
  post '/deployment', :to => 'deployment#post'  
end
