# parameters
include .env.dev
.DEFAULT_GOAL := info
export $(shell sed 's/=.*//' .env.dev)


info:
	@echo "Make-Sauron :"
	@echo "			- dev (launch development server)"
	@echo "			- update (fetch latest Gems)"
	@echo "			- assets (push Docker image)"
	@echo "			- migrate (init or migrate DB)"

	@echo
	
# Launch Dev environment
dev:
	bundle exec bin/rails s

assets:
	bundle
	yarn install --check-file	
	bundle exec bin/rails assets:clean

migrate:
	bundle exec bin/rails db:migrate

update:
	bundle update
	yarn upgrade --check-file


build:  
	sudo docker build -t $(IMAGE_NAME) .

