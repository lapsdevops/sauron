class WelcomeController < ApplicationController
  
  def index
    show_vms

    @ipv4Enabled = Rails.configuration.application['ENABLE_IPV4']
    @ipv6Enabled = Rails.configuration.application['ENABLE_IPV6']
    @cf_node = Rails.configuration.application['NETBOX_NODE_CF']
    @cf_vmid = Rails.configuration.application['NETBOX_PVEVMID_CF']

  end


  def show_vms

    clusters = []
    clusters_id = []
    for cluster in  Rails.configuration.application['CLUSTERS'] 
      clusters << cluster[0]
      clusters_id << cluster[1][:NETBOX_VM_CLUSTER_ID]
    end


    NetboxClientRuby.configure do |c|
      c.netbox.auth.token = current_user.netbox_token    
      c.netbox.api_base_url = Rails.configuration.application['NETBOX_API_URL']
      c.netbox.pagination.default_limit = 1000
    end

    @vms_by_cluster = []
    @vms_total = 0
    begin
      for cluster_id in clusters_id
        vms = NetboxClientRuby.virtualization.virtual_machines.filter(cluster_id: cluster_id, cf_terraformable: 1)
        @vms_by_cluster << vms
      end
      for vms_by_cluster in @vms_by_cluster
        @vms_total += vms_by_cluster.total
      end
      @cluster_name = clusters.join(", ")
      #puts @cluster_name

    rescue => error
      puts "ERROR"
      flash[:error] = "Impossible de se connecter à Netbox."
      @cluster_name = nil
    end
  end   
  
  
end
