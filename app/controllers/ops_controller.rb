class OpsController < ApplicationController

    def index
        @mmonit_url = Rails.configuration.application['MMONIT_HOST']
        getRundeckLastFailed
    end

    def getRundeckLastFailed
        begin
            # Let's trigger our Rundeck jobs
            require 'rundeck'
            rundeck_url = Rails.configuration.application['RUNDECK_URL']
            options = { :endpoint => rundeck_url, :api_token => current_user.rundeck_token }
            rd = Rundeck::Client.new(options)
            ssl_verify = {:verify => false}
            out = rd.running_job_executions('Global')
            @rundeckExec = out.to_h
            @rundeck_url = rundeck_url.to_s

        rescue => exception
            flash[:error] = "Unable to fetch Rundeck job."
        end     
    end

end
