class EditController < ApplicationController

    def index
        getDatas(params[:id])
        getSaltRole
        getProxmoxPool        
    end


    def getDatas(id)
        NetboxClientRuby.configure do |c|
        c.netbox.auth.token = current_user.netbox_token    
        c.netbox.api_base_url = Rails.configuration.application['NETBOX_API_URL']
        end
        cluster_id = Rails.configuration.application['NETBOX_VM_CLUSTER_ID']
        begin
            cluster_name = NetboxClientRuby.virtualization.cluster(cluster_id)
            @cluster_name = cluster_name.name

            @vm = NetboxClientRuby.virtualization.virtual_machine(id)
            #puts vm

        rescue => error
            flash[:error] = "Impossible de se connecter à Netbox."
            @cluster_name = nil
        end
    end  
end
