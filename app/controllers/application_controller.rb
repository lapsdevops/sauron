
class ApplicationController < ActionController::Base

  rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
    render :text => exception, :status => 500
  end

    before_action :authenticate_user!
    protect_from_forgery with: :exception
    before_action :configure_permitted_parameters, if: :devise_controller?
  
    def get_vm(vmid)
    
      clusters = []
      clusters_id = []
      #begin
        NetboxClientRuby.configure do |c|
          c.netbox.auth.token = current_user.netbox_token    
          c.netbox.api_base_url = Rails.configuration.application['NETBOX_API_URL']
        end

      for cluster in  Rails.configuration.application['CLUSTERS'] 
        clusters << cluster[0]
        clusters_id << cluster[1][:NETBOX_VM_CLUSTER_ID]
      end
      @vms_by_cluster = []
      for cluster_id in clusters_id
        vms = NetboxClientRuby.virtualization.virtual_machines.filter(cluster_id: cluster_id, id: vmid)
        @vms_by_cluster << vms
      end
      #rescue => error
          #flash[:error] = "Unable to fetch VM on Netbox."
          #@vms = nil
      #end
  
      @vms = vms
    end
  
    def get_nodes(url)
      proxmoxUrl = url
      proxmoxLoginType = Rails.configuration.application['PROXMOX_LOGIN_TYPE']
      proxmoxApiUrl = "https://" + proxmoxUrl + "/api2/json/"
      require 'proxmox'
      pveUser = current_user.proxmox_user
      pvePass = current_user.proxmox_password
      begin
        proxmox = Proxmox::Proxmox.new(proxmoxApiUrl, "localhost", pveUser, pvePass, Rails.configuration.application['PROXMOX_LOGIN_TYPE'], {verify_ssl: false})
        nodes = proxmox.get("/nodes")
      rescue
        flash[:error] = "Unable to fetch Proxmox nodes."
        nodes = nil
      end
    end
    helper_method :get_nodes
            
    def get_mmonit_status(vmid)
      begin
        require 'mmonit'
        require 'json'
        mmonit_username = current_user.mmonit_username
        mmonit_password = current_user.mmonit_password
        mmonit_url = Rails.configuration.application['MMONIT_HOST']
        mmonit = MMonit::Connection.new({ :ssl => true, :username => mmonit_username, :password => mmonit_password, :address => mmonit_url, :port => '443' })
        mmonit.connect
        hosts = mmonit.hosts
        s = NetboxClientRuby.virtualization.virtual_machine(vmid)
        vm_name = s.name
        for host in hosts
          hostname =  host['hostname']
          if hostname == vm_name
            mmonit_host_id = host['id']
          end
        end
        path = "/status/hosts/get?id=" + mmonit_host_id.to_s
        mmonit.connect
        stats = mmonit.request(path)
        mmonit_status_all = stats.body
        require 'json'
        json = JSON.parse(mmonit_status_all)
        rec = json["records"]["host"]
        @services = rec["services"]
        return @services
  
      rescue => exception
        flash[:error] = "Unable to fetch M/Monit status."
        @services = nil
      end
    end
    helper_method :get_mmonit_status

    def get_mmonit_alerts()
      begin
        require 'mmonit'
        require 'json'
        mmonit_username = current_user.mmonit_username
        mmonit_password = current_user.mmonit_password
        mmonit_url = Rails.configuration.application['MMONIT_HOST']
        mmonit = MMonit::Connection.new({ :ssl => true, :username => mmonit_username, :password => mmonit_password, :address => mmonit_url, :port => '443' })
     
        path = "/reports/events/list?active=1"
        mmonit.connect
        alerts_brut = mmonit.request(path)
        alerts_j = alerts_brut.body
        require 'json'
        json = JSON.parse(alerts_j)
        alerts = json["records"]
        return alerts
  
      rescue => exception
        flash[:error] = "Unable to fetch M/Monit alerts."
        @services = nil
      end
    end
    helper_method :get_mmonit_alerts
        
    def get_mmonit_alerts_for_vm(vmname)
      begin
        require 'mmonit'
        require 'json'
        mmonit_username = current_user.mmonit_username
        mmonit_password = current_user.mmonit_password
        mmonit_url = Rails.configuration.application['MMONIT_HOST']
        mmonit = MMonit::Connection.new({ :ssl => true, :username => mmonit_username, :password => mmonit_password, :address => mmonit_url, :port => '443' })
        mmonit.connect
        hosts = mmonit.hosts
        for host in hosts
          hostname =  host['hostname']
          if hostname == vmname
            mmonit_host_id = host['id']
          end
        end
        if mmonit_host_id != nil
          path = "/reports/events/list?active=1&hostid=" + mmonit_host_id.to_s
          mmonit.connect
          alerts_brut = mmonit.request(path)
          alerts_j = alerts_brut.body
          require 'json'
          json = JSON.parse(alerts_j)
          alerts = json["records"]
          return alerts
        else
          alerts = []
        end          
      rescue => exception
        alerts = []
        return alerts
      end
    end
    helper_method :get_mmonit_alerts_for_vm

    def delete_mmonit_for_vm(vmname)
      begin
        require 'mmonit'
        require 'json'
        mmonit_username = current_user.mmonit_username
        mmonit_password = current_user.mmonit_password
        mmonit_url = Rails.configuration.application['MMONIT_HOST']
        mmonit = MMonit::Connection.new({ :ssl => true, :username => mmonit_username, :password => mmonit_password, :address => mmonit_url, :port => '443' })
        mmonit.connect
        hosts = mmonit.hosts
        for host in hosts
          hostname =  host['hostname']
          if hostname == vmname
            mmonit_host_id = host['id']
          end
        end
        if mmonit_host_id != nil
          mmonit.connect
          ret = mmonit.delete_host(mmonit_host_id.to_s)
        else
          flash[:error] = "Unable to delete VM on M/Monit."
        end          
      rescue => exception
        flash[:error] = "Unable to delete VM on M/Monit."
      end
    end

    def get_clusters()

      clusters = []
      for cluster in  Rails.configuration.application['CLUSTERS'] 
        clusters << cluster[0]
      end

      return clusters
    end
      helper_method :get_clusters

   def get_url_for_cluster(cluster_name)

    for cluster in Rails.configuration.application['CLUSTERS']
      clu = cluster[0]
      if clu.to_s == cluster_name
        url = cluster[1][:PROXMOX_URL]
      end
    end
    return url

   end
      helper_method :get_url_for_cluster

   def get_vm_status(id, cluster_name)
      require 'proxmox'
      pveUser = current_user.proxmox_user
      pvePass = current_user.proxmox_password
      url = ""
      for cluster in Rails.configuration.application['CLUSTERS']
        clu = cluster[0]
        if clu.to_s == cluster_name
          url = cluster[1][:PROXMOX_URL]
        end
      end
      proxmoxUrl = url
      proxmoxLoginType = Rails.configuration.application['PROXMOX_LOGIN_TYPE']
      proxmoxApiUrl = "https://" + proxmoxUrl + "/api2/json/"
      nodes = get_nodes(proxmoxUrl)
      @status == nil
      begin
        for node in nodes         
          server1 = Proxmox::Proxmox.new(proxmoxApiUrl, node.to_h["node"], pveUser, pvePass, proxmoxLoginType, {verify_ssl: false})
          cur = server1.qemu_current(id)
          next if cur["status"] == nil
          @status = cur["status"]
        end
        if @status == nil
            flash[:error] = "La VM n'existe pas dans Proxmox."
        end
        return @status
      rescue => error
        flash[:error] = "Unable to connect to Proxmox."
        @status = nil
      end  
   end      
    helper_method :get_vm_status
  
    def getSaltRole
      begin
          require 'httparty'
          require 'json'
          cf_roles = Rails.configuration.application['NETBOX_ROLES_CF']
          url = Rails.configuration.application['NETBOX_API_URL'] + 'extras/custom-fields/?name=' + cf_roles  
          headers = {
          Authorization: 'Token ' + current_user.netbox_token
          }
          req = HTTParty.get(url, headers: headers)
          roles = JSON.parse(req.body)

          for role in roles['results']
            choices = role['choice_set']
          end
          url = choices['url']
          headers = {
          Authorization: 'Token ' + current_user.netbox_token
          }
          req = HTTParty.get(url, headers: headers)
          ret = JSON.parse(req.body)
          @saltRoles = ret['extra_choices'].flatten.uniq






      rescue => error
          flash[:error] = "Impossible de récupérer les CustomFields Netbox."
      end
    end

    def get_gitlab_pipe_url
      gitlab_pipe_url = Rails.configuration.application['GITLAB_PIPELINE_URL']
      return gitlab_pipe_url
    end
      helper_method :get_gitlab_pipe_url

    def getProxmoxPool
        begin
            require 'httparty'
            require 'json'
            cf_pool = Rails.configuration.application['NETBOX_POOL_CF']
            url = Rails.configuration.application['NETBOX_API_URL'] + 'extras/custom-fields/?name=' + cf_pool 
            headers = {
            Authorization: 'Token ' + current_user.netbox_token
            }
            req = HTTParty.get(url, headers: headers)
            pools = JSON.parse(req.body)
            for choice in pools['results']
              choices = choice['choice_set']
            end
            url = choices['url']
            headers = {
            Authorization: 'Token ' + current_user.netbox_token
            }
            req = HTTParty.get(url, headers: headers)
            ret = JSON.parse(req.body)
            @ProxmoxPool = ret['extra_choices'].flatten.uniq

        rescue => error
            flash[:error] = "Impossible de récupérer les CustomFields Netbox."
        end
    end

    def getProxmoxOS
      begin
          require 'httparty'
          require 'json'
          os_desc = Rails.configuration.application['NETBOX_OSTEMPLATE_DESC']
          url = Rails.configuration.application['NETBOX_API_URL'] + 'dcim/platforms/?description=' + os_desc 
          headers = {
          Authorization: 'Token ' + current_user.netbox_token
          }
          req = HTTParty.get(url, headers: headers)
          @ProxmoxOS = JSON.parse(req.body)
          result = req.body
      rescue => error
          flash[:error] = "Impossible de récupérer les ProxmoxOS dans Netbox."
      end
    end     

    protected
  
      def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :password])
        #devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
        devise_parameter_sanitizer.permit(:account_update, keys: [:username, :email, :password, :current_password, :rundeck_token, :netbox_token, :proxmox_user, :proxmox_password, :mmonit_username, :mmonit_password])
      end
    
      
end
