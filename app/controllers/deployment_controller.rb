# Encoding: utf-8

class DeploymentController < ApplicationController
  def index

    g = Gitlab.client(
      endpoint: Rails.configuration.application['GITLAB_URL'],
      private_token: Rails.configuration.application['GITLAB_TOKEN']
    )

    pipelines = g.pipelines(Rails.configuration.application['GITLAB_TERRA_PROJECT'], {per_page:1})
    
    for pipeline in pipelines
      latestPipelineID = pipeline['id']
    end

    if latestPipelineID != nil
      latestpipe = g.pipeline_jobs(Rails.configuration.application['GITLAB_TERRA_PROJECT'], latestPipelineID)
    end

    for job in latestpipe  
    

      if job['stage'] == "plan"
        testid = job['id']
        rawtest = g.job_trace(Rails.configuration.application['GITLAB_TERRA_PROJECT'], testid) 

        if rawtest != nil 
          @test = rawtest
          .gsub!(/section_end:[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]:/, '')
          .gsub!(/section_start:[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]:/, '')

        end

      end

      if job['stage'] == "deploy"
        @deployid = job['id']  
        deployClean = g.job_trace(Rails.configuration.application['GITLAB_TERRA_PROJECT'], @deployid)
        
        if job['started_at'] != nil
          if deployClean != nil
            rawdeploy = deployClean.force_encoding('UTF-8')
          else
            rawdeploy = deployClean
          end 

          if rawdeploy != nil
            @deploy = rawdeploy
            .gsub!(/section_end:[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]:/, '')
            .gsub!(/section_start:[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]:/, '')
          end
        end
     end      
    end
 

  end

  def post

    g = Gitlab.client(
      endpoint: Rails.configuration.application['GITLAB_URL'],
      private_token: Rails.configuration.application['GITLAB_TOKEN']
    )

    deployJobID = params[:jobID]
    ret = g.job_play(Rails.configuration.application['GITLAB_TERRA_PROJECT'], deployJobID)
    puts 'Job ' + deployJobID + ' launched.'

    sleep(5)
    redirect_to :controller => 'deployment', :action => 'index'
  end

end
